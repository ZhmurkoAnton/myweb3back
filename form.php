<!DOCTYPE html> <html lang="ru"> <head>
  <link rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script
			src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="style.css">
    <title> Форма Жмурко </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width= device-width, initial-scale=1.0">
  </head>
  <body>
    <div class="px-0 px-sm-3 container">
</div>
  <div class="form container col-12 mt-3 mb-0">
    <section id="form"> <h2>Форма </h2>
  <form action="."
    method="POST">
    <label>
      Имя<br />
      <input name="name"
        value="Антон Сергеевич"/>
    </label><br />
<br/>
    <label>
      E-mail:<br />
      <input name="email"
        value="qwerty03441@gmail.com"
        type="email" />
    </label><br />
<br/>
    <label>
      Дата рождения:<br />
      <input name="birthday"
        value="2001-10-28"
        type="date" />
    </label><br />
<br/>
    Пол:<br />
    <label><input type="radio" checked="checked"
      name="gender" value="M" />
      мужской
    </label>
    <label><input type="radio"
      name="gender" value="F" />
      женский
    </label>
    <br/>
    <br/>
    Количество конечностей:<br />
    <label><input type="radio"
      name="limbs" value="0" />
     0
    </label>

    <label><input type="radio"
      name="limbs" value="1" />
      1
    </label>

    <label><input type="radio"
      name="limbs" value="2" />
      2
    </label> 

    <label><input type="radio" checked="checked"
      name="limbs" value="4" />
      4
    </label>
    <label>
    	<input type="radio"
    	name="limbs" value="more than 8"/>
    	8+
    </label>
<br/>
    <br />
    <label>
      Сверхспособности:
      <br />
      <select name="abilities[]"
        multiple="multiple">
        <option value="1">Бессмертие</option>
        <option value="2" selected="selected">Прохождение сквозь стены</option>
         <option value="3" selected="selected">Невидимость</option>
        <option value="4"> Левитация</option>
        <option value="5" selected="selected">Мгновенная телепортация</option>
      </select>
    </label><br />
<br/>
    <label>
      Биография:<br />
      <textarea name="biography" placeholder="Заинтересуйте нас" rows="5" cols="45"> Послушайте - когда-то, две жены тому назад, двести пятьдесят тысяч сигарет тому назад, три тысячи литров спиртного тому назад...Тогда, когда все были молоды...</textarea>
    </label><br />

    <br />
    <label><input type="checkbox"
      name="contract" />
      С контрактом ознакомлен</label><br />
      <br/>
    <a id= "submit_button"></a>
    <input type="submit" value="Отправить" />
  </form>
</section>
</div>
    </div>
    </div>
  </body>
</html>
